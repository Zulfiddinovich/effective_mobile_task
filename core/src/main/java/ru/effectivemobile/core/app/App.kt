package ru.effectivemobile.core.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 09:11
   Project: EffectiveMobile Task
*/

@HiltAndroidApp
class App @Inject constructor(): Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object{
        lateinit var instance: App
            private set


    }

}