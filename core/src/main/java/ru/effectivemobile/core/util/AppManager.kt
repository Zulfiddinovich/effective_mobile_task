package ru.effectivemobile.core.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import ru.effectivemobile.core.app.App
import java.util.*
import java.util.concurrent.TimeUnit


/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

/*
 * PD and PX
 */
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Fragment.hideKeyBoard(){
    val view: View? = this.requireActivity().currentFocus
    if (view != null) {
        val imm: InputMethodManager = this.requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}


    fun Long.toDate(): String {

    val offset: Int = TimeZone.getDefault().rawOffset + TimeZone.getDefault().dstSavings
    val now: Long = this + offset

    var day: Long = TimeUnit.MILLISECONDS.toDays(now)
    var hr: Long = TimeUnit.MILLISECONDS.toHours(now)
    val min: Long = TimeUnit.MILLISECONDS.toMinutes(now - TimeUnit.HOURS.toMillis(hr))
//        val sec: Long = TimeUnit.MILLISECONDS.toSeconds(now - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min))
    //val ms: Long = TimeUnit.MILLISECONDS.toMillis(now - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(sec))
    hr = hr - (hr/24) * 24

//        val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("d/M/u")
//        val startDate = "1/4/2022"
//        val endDate = "14/5/2022"
//        val startDateValue: LocalDate = LocalDate.parse(startDate, dateFormatter)
//        val endDateValue: LocalDate = LocalDate.parse(endDate, dateFormatter)
//        val days: Long = ChronoUnit.DAYS.between(startDateValue, endDateValue) + 1
//        println("Days: $days")
//        return days.toString()
    return String.format("%02d:%02d", hr, min)

}

fun setClipboard(text: String) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        val clipboard = App.instance.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
        clipboard.text = text
    } else {
        val clipboard = App.instance.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
    }
    Toast.makeText(App.instance, "Copied \"$text\"", Toast.LENGTH_SHORT).show()
}