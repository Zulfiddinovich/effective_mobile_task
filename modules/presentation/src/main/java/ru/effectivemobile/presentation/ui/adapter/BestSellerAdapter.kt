package ru.effectivemobile.presentation.ui.adapter

import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.effectivemobile.data.model.common.BestSellerData
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.BestSellerItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class BestSellerAdapter : ListAdapter<BestSellerData, BestSellerAdapter.BestSellerViewHolder>(
    DifUtil
) {
    private var selectListener: ((BestSellerData) -> Unit)? = null
    private var favListener: ((BestSellerData) -> Unit)? = null


    object DifUtil : DiffUtil.ItemCallback<BestSellerData>() {
        override fun areItemsTheSame(oldItem: BestSellerData, newItem: BestSellerData): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: BestSellerData, newItem: BestSellerData): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    inner class BestSellerViewHolder(private val binding: BestSellerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.bestsellerItem.setOnClickListener {
                selectListener?.invoke(getItem(absoluteAdapterPosition))
            }

            binding.bestsellerFavContainer.setOnClickListener {
                favListener?.invoke(getItem(absoluteAdapterPosition))
                if (getItem(absoluteAdapterPosition).is_favorites) {
                    binding.favImage.setImageResource(R.drawable.favorite)
                } else {
                    binding.favImage.setImageResource(R.drawable.favorite_border)
                }
            }
        }

        fun onBind() {
            val item = getItem(absoluteAdapterPosition)

            Glide.with(binding.bestsellerImage)
                .load(item.picture)
                .into(binding.bestsellerImage)

            binding.newPrice.text = item.discount_price.toString()
            binding.oldPrice.text = item.price_without_discount.toString()
            binding.oldPrice.paintFlags = binding.oldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            binding.title.text = item.title

            Log.d("TAG", "onBind: is_favorites: " + item.is_favorites)
            if (item.is_favorites) {
                binding.favImage.setImageResource(R.drawable.favorite)
            } else {
                binding.favImage.setImageResource(R.drawable.favorite_border)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder =
        BestSellerViewHolder(
            BestSellerItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.onBind()
    }

    fun selectListenerSet(listener: (BestSellerData) -> Unit) {
        this.selectListener = listener
    }
    fun favListenerSet(listener: (BestSellerData) -> Unit) {
        this.favListener = listener
    }




}