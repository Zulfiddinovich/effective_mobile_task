package ru.effectivemobile.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.effectivemobile.data.model.common.SalesData
import ru.effectivemobile.presentation.databinding.SalesItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class SalesAdapter: ListAdapter<SalesData, SalesAdapter.SalesViewHolder>(DifUtil) {
    private var buyNowListener: ((SalesData) -> Unit)? = null


    object DifUtil: DiffUtil.ItemCallback<SalesData>() {
        override fun areItemsTheSame(oldItem: SalesData, newItem: SalesData): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: SalesData, newItem: SalesData): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    inner class SalesViewHolder(private val binding: SalesItemBinding): RecyclerView.ViewHolder(binding.root){
        init {
            binding.buyNow.setOnClickListener {
                buyNowListener?.invoke(getItem(absoluteAdapterPosition))
            }
        }

        fun onBind(){
            val item = getItem(absoluteAdapterPosition)

            Glide.with(binding.salesImage)
                .load(item.picture)
                .into(binding.salesImage)

            binding.title.text = item.title
            binding.isNewContainer.isVisible = item.is_new ?: false

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalesViewHolder =
        SalesViewHolder(SalesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: SalesViewHolder, position: Int) {
        holder.onBind()
    }

    fun butNowListenerSet(listener: (SalesData) -> Unit){
        this.buyNowListener = listener
    }


}