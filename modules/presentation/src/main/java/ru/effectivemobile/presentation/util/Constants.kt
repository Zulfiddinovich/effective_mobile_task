package ru.effectivemobile.presentation.util

import androidx.lifecycle.MutableLiveData
import ru.effectivemobile.data.model.common.BadgeData
import ru.effectivemobile.presentation.R

/*
   Author: Zukhriddin Kamolov
   Created: 16.11.2022 at 14:50
   Project: EffectiveMobile Task
*/

class Constants {

    companion object {
        val badgeState = listOf(
            BadgeData(  0, R.id.explorer),
            BadgeData(5, R.id.cart),
            BadgeData(10, R.id.favourite),
            BadgeData(1, R.id.account),
        )

        val goToCartLivedata = MutableLiveData<Boolean>()
        val chosenColorPositionLivedata = MutableLiveData<Int>()
        val chosenCapacityPositionLivedata = MutableLiveData<Int>()
        val addToCartLivedata = MutableLiveData<Boolean>()
    }
}