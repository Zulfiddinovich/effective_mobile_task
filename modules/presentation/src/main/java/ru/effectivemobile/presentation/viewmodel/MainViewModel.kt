package ru.effectivemobile.core.presentation.viewmodel

import androidx.lifecycle.LiveData
import ru.effectivemobile.data.model.common.BestSellerData
import ru.effectivemobile.data.model.common.CategoryData1
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.data.model.common.SalesData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 12:10
   Project: EffectiveMobile Task
*/
interface MainViewModel {
    val progressLiveData: LiveData<Boolean>
    val messageLiveData: LiveData<String>
    val categoryListLiveData: LiveData<List<CategoryParent>>
    val saleListLiveData: LiveData<List<SalesData>>
    val bestSellerListLiveData: LiveData<List<BestSellerData>>

    val startFilteringLiveData: LiveData<Unit>
    val selectCategoryLiveData: LiveData<CategoryParent>
    val searchByQueryLiveData: LiveData<String>
    val buyItNowLiveData: LiveData<SalesData>
    val bestSellerItemLiveData: LiveData<BestSellerData>
    val changeFavouriteItemLiveData: LiveData<BestSellerData>



    fun getCategoryList()

    fun getHotSalesList()

    fun getBestSellerList()

    fun startFiltering()

    fun viewAllCategory()
    fun selectedCategory(category: CategoryParent)

    fun searchByQuery(query: String)

    fun seeMoreHotSales()
    fun buyNowItem(item: SalesData)

    fun bestSellerItem(item: BestSellerData)
    fun seeMoreBestSeller()

    fun changeFavouriteItem(item: BestSellerData)



}
