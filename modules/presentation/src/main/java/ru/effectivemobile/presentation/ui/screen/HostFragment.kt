package ru.effectivemobile.presentation.ui.screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ru.effectivemobile.data.model.common.MyEventBus
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FragmentHostBinding
import ru.effectivemobile.presentation.util.Constants.Companion.badgeState
import ru.effectivemobile.presentation.util.Constants.Companion.goToCartLivedata

@AndroidEntryPoint
class HostFragment : Fragment(R.layout.fragment_host) {
    private val binding by viewBinding(FragmentHostBinding::bind)
    private val mainFragment = HomeFragment()
    private val cartFragment = CartFragment()
    private var current: Fragment = mainFragment


    override fun onViewCreated(view: View, savedInstanceState: Bundle?): Unit = with(binding) {
        bottomNavInit()
        clicks()
        goToCartLivedata.observe(viewLifecycleOwner, goToCartObserver)


    }

    private val goToCartObserver = Observer<Boolean>{
        if (it) {
            replaceFragment(R.id.cart)
            goToCartLivedata.value = false
        }
    }

    private fun clicks() {
        binding.bottomNavBar.setOnItemSelectedListener { id ->
            replaceFragment(id)
        }

    }

    @Subscribe
    fun onEvent(event: MyEventBus) {
        when (event) {
            is MyEventBus.SeeAllCategoryEvent -> { /**/
            }
            is MyEventBus.SeeHotSalesEvent -> { /**/
            }
            is MyEventBus.BuyNowEvent -> {
                findNavController().navigate(HostFragmentDirections.actionHostFragmentToProductDetailFragment())
            }
            is MyEventBus.SeeBestSellersEvent -> { /**/
            }
            is MyEventBus.BestSellerClickEvent -> {
                findNavController().navigate(HostFragmentDirections.actionHostFragmentToProductDetailFragment())
            }
            is MyEventBus.MoveToCartEvent -> {
                replaceFragment(event.fragmentIdMenu)
            }
            is MyEventBus.MoveToExplorerEvent -> {
                replaceFragment(event.fragmentIdMenu)
            }

            is MyEventBus.CartBadgeRefresh -> {
                badgeState[1].isVisible = event.data.isVisible
                badgeState[1].count = event.data.count
                badgeRefresh()
            }
        }
    }

    private fun bottomNavInit() {
        badgeRefresh()
        binding.bottomNavBar.setItemSelected(R.id.explorer)
        if (!childFragmentManager.fragments.contains(mainFragment)) {
            current = mainFragment
            childFragmentManager.beginTransaction()
                .add(R.id.container_view, cartFragment).hide(cartFragment)
                .add(R.id.container_view, mainFragment).commit()
        }
    }

    private fun badgeRefresh(){
        badgeState.onEach {
            if (it.isVisible) binding.bottomNavBar.showBadge(it.resId, it.count)
            else binding.bottomNavBar.dismissBadge(it.resId)
        }
    }

    private fun replaceFragment(fragmentIdMenu: Int){
        childFragmentManager.beginTransaction().apply {
            binding.bottomNavBar.setItemSelected(fragmentIdMenu)
            when (fragmentIdMenu) {
                R.id.explorer -> {
                    this.hide(current).show(mainFragment).commit()
                    current = mainFragment
                }
                R.id.cart -> {
                    this.hide(current).show(cartFragment).commit()
                    current = cartFragment
                }
                R.id.favourite -> {}
                R.id.account -> {}
                else -> {}
            }
        }
    }


    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }


}