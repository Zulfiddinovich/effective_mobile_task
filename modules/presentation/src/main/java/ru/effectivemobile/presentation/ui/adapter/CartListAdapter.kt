package ru.effectivemobile.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.presentation.databinding.CartItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class CartListAdapter: ListAdapter<BasketItemData, CartListAdapter.CartListViewHolder>(DifUtil) {
    private var incrementListener: ((BasketItemData, position: Int) -> Unit)? = null
    private var decrementListener: ((BasketItemData, position: Int) -> Unit)? = null
    private var deleteListener: ((BasketItemData, position: Int) -> Unit)? = null
    private var refreshListener: ((Unit) -> Unit)? = null


    object DifUtil: DiffUtil.ItemCallback<BasketItemData>() {
        override fun areItemsTheSame(oldItem: BasketItemData, newItem: BasketItemData): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BasketItemData, newItem: BasketItemData): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    inner class CartListViewHolder(private val binding: CartItemBinding): RecyclerView.ViewHolder(binding.root){
        init {
            binding.plus.setOnClickListener {
                incrementListener?.invoke(getItem(absoluteAdapterPosition), absoluteAdapterPosition)
            }
            binding.minus.setOnClickListener {
                decrementListener?.invoke(getItem(absoluteAdapterPosition), absoluteAdapterPosition)
            }
            binding.delete.setOnClickListener {
                deleteListener?.invoke(getItem(absoluteAdapterPosition), absoluteAdapterPosition)
            }
        }

        fun onBind(){
            val item = getItem(absoluteAdapterPosition)

            Glide.with(binding.itemImage).load(item.images).into(binding.itemImage)

            binding.itemTitle.text = item.title
            binding.count.text = item.counter.toString()
            binding.itemPriceSum.text = "$" + (item.price * item.counter)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartListViewHolder =
        CartListViewHolder(CartItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: CartListViewHolder, position: Int) {
        holder.onBind()
    }

    fun incrementListenerSet(listener: (BasketItemData, position: Int) -> Unit){
        this.incrementListener = listener
    }
    fun decrementListenerSet(listener: (BasketItemData, position: Int) -> Unit){
        this.decrementListener = listener
    }
    fun deleteListenerSet(listener: (BasketItemData, position: Int) -> Unit){
        this.deleteListener = listener
    }
    fun refreshListenerSet(listener: (Unit) -> Unit){
        this.refreshListener = listener
    }

    // below function triggers refreshListener,
    // then refreshListener calls setTotalSumValue() fun in CartFragment
    override fun onCurrentListChanged(
        previousList: MutableList<BasketItemData>,
        currentList: MutableList<BasketItemData>
    ) {
        refreshListener?.invoke(Unit)
        super.onCurrentListChanged(previousList, currentList)
    }


}