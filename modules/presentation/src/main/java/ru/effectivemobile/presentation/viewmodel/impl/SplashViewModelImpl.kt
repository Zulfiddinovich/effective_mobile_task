package ru.effectivemobile.presentation.viewmodel.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.effectivemobile.presentation.viewmodel.SplashViewModel
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 18.11.2022 at 9:44
   Project: EffectiveMobile Task
*/
@HiltViewModel
class SplashViewModelImpl @Inject constructor() : ViewModel(), SplashViewModel {
    override val goMainScreenLiveData = MutableLiveData<Unit>()

    override fun goMainScreen() {
        viewModelScope.launch {
            delay(2000)
            goMainScreenLiveData.value = Unit
        }
    }


}