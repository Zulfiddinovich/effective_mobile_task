package ru.effectivemobile.presentation.ui.screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FragmentSplashBinding
import ru.effectivemobile.presentation.viewmodel.SplashViewModel
import ru.effectivemobile.presentation.viewmodel.impl.SplashViewModelImpl

@AndroidEntryPoint
class SplashFragment : Fragment(R.layout.fragment_splash) {
    private val viewModel: SplashViewModel by viewModels<SplashViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.goMainScreen()

        viewModel.goMainScreenLiveData.observe(viewLifecycleOwner){
            findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToHostFragment())
        }
    }

}