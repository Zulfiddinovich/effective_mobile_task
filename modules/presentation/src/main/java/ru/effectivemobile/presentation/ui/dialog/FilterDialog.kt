package ru.effectivemobile.presentation.ui.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.google.android.material.bottomsheet.BottomSheetDialog
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FilterLayoutBinding

/*
   Author: Zukhriddin Kamolov
   Created: 15.11.2022 at 3:34
   Project: EffectiveMobile Task
*/

class FilterDialog(val context: Context) {

    private lateinit var bottomSheetBinding: FilterLayoutBinding

    val bottomSheet = BottomSheetDialog(context, R.style.sheet_dialog_style)

    var brand = ""
        private set
    var price = ""
        private set
    var size = ""
        private set

    private val brandList = arrayOf("Samsung", "Apple", "Xiaomi", "Nokia", "Others")
    private val brandAdapter by lazy {
        ArrayAdapter(context, R.layout.my_spinner_item, brandList).apply {
            setDropDownViewResource(R.layout.my_spinner_dropdown_item)
        }
    }
    private val priceList = arrayOf(
        "$100 - $300",
        "$300 - $500",
        "$500 - $800",
        "$800 - $1 300",
        "$1 300 - $2 000",
        "$2 000 - $5 000",
        "$5 000 - $10 000"
    )
    private val priceAdapter by lazy {
        ArrayAdapter(context, R.layout.my_spinner_item, priceList).apply {
            setDropDownViewResource(R.layout.my_spinner_dropdown_item)
        }
    }
    private val sizeList =
        arrayOf("3.5 to 4.5 inches", "4.5 to 5.5 inches", "5.5 to 6.5 inches", "6.5 to 7.6 inches")
    private val sizeAdapter by lazy {
        ArrayAdapter(context, R.layout.my_spinner_item, sizeList).apply {
            setDropDownViewResource(R.layout.my_spinner_dropdown_item)
        }
    }

    init {
        bottomSheetInit()
        init()
        clicks()
    }

    private fun init(){
        (bottomSheetBinding.brandSpinner.getChildAt(0) as Spinner).apply {
            onItemSelectedListener = brandListener
            adapter = brandAdapter
        }

        (bottomSheetBinding.priceSpinner.getChildAt(0) as Spinner).apply {
            onItemSelectedListener = priceListener
            adapter = priceAdapter
        }

        (bottomSheetBinding.sizeSpinner.getChildAt(0) as Spinner).apply {
            onItemSelectedListener = sizeListener
            adapter = sizeAdapter
        }
    }


    private fun bottomSheetInit(): BottomSheetDialog {
        bottomSheetBinding = FilterLayoutBinding.inflate(LayoutInflater.from(context), null, false)
        bottomSheet.apply {
            setContentView(bottomSheetBinding.root)
            setOnCancelListener {
                (bottomSheetBinding.brandSpinner.getChildAt(0) as Spinner).setSelection(0)
                (bottomSheetBinding.priceSpinner.getChildAt(0) as Spinner).setSelection(0)
                (bottomSheetBinding.sizeSpinner.getChildAt(0) as Spinner).setSelection(0)
                brand = (bottomSheetBinding.brandSpinner.getChildAt(0) as Spinner).adapter.getItem(0)
                        .toString()
                price = (bottomSheetBinding.priceSpinner.getChildAt(0) as Spinner).adapter.getItem(0)
                        .toString()
                size = (bottomSheetBinding.sizeSpinner.getChildAt(0) as Spinner).adapter.getItem(0)
                    .toString()
            }
        }
        return bottomSheet
    }


    private fun clicks(){
        bottomSheetBinding.cancel.setOnClickListener {
            bottomSheet.cancel()
        }

        bottomSheetBinding.done.setOnClickListener {
//            Toast.makeText(requireContext(), "$brand, $price, $size ", Toast.LENGTH_SHORT).show()
            bottomSheet.dismiss()
            // todo
        }

    }


    private val brandListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            brand = brandList[position]
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {

        }
    }
    private val priceListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            price = priceList[position]
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {

        }
    }
    private val sizeListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            size = sizeList[position]
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {

        }
    }


}