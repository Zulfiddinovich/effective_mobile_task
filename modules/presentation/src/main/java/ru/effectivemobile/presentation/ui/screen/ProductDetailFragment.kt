package ru.effectivemobile.presentation.ui.screen

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import com.jackandphantom.carouselrecyclerview.CarouselLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import ru.effectivemobile.core.presentation.viewmodel.DetailedProductViewModel
import ru.effectivemobile.core.presentation.viewmodel.impl.DetailedProductViewModelImpl
import ru.effectivemobile.data.model.common.BadgeData
import ru.effectivemobile.data.model.common.MyEventBus
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FragmentProductDetailBinding
import ru.effectivemobile.presentation.ui.adapter.CarouselAdapter
import ru.effectivemobile.presentation.ui.adapter.DetailsPagerAdapter
import ru.effectivemobile.presentation.ui.pages.DetailsPage
import ru.effectivemobile.presentation.ui.pages.FeaturesPage
import ru.effectivemobile.presentation.ui.pages.ShopPage
import ru.effectivemobile.presentation.util.Constants.Companion.addToCartLivedata
import ru.effectivemobile.presentation.util.Constants.Companion.badgeState
import ru.effectivemobile.presentation.util.Constants.Companion.chosenCapacityPositionLivedata
import ru.effectivemobile.presentation.util.Constants.Companion.chosenColorPositionLivedata
import ru.effectivemobile.presentation.util.Constants.Companion.goToCartLivedata

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class ProductDetailFragment : Fragment(R.layout.fragment_product_detail) {
    private val binding by viewBinding(FragmentProductDetailBinding::bind)
    private val viewModel: DetailedProductViewModel by viewModels<DetailedProductViewModelImpl>()

    private val productAdapter by lazy { CarouselAdapter() }
    private val pagerAdapter by lazy { DetailsPagerAdapter(childFragmentManager, lifecycle) }
    private val shopPage by lazy { ShopPage() }
    private val detailsPage by lazy { DetailsPage() }
    private val featuresPage by lazy { FeaturesPage() }
    private var currentPosition = 0
    private var chosenColorPosition = 0
    private var chosenCapacityPosition = 0
    private var products: List<ProductData>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.carousel.scrollToPosition(currentPosition)
        binding.detailViewpager.adapter = pagerAdapter
        setData(currentPosition)
        clicks()
        adapters()
        liveDatas()


    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun liveDatas() {
        viewModel.progressLiveData.observe(viewLifecycleOwner, progressObserver)
        viewModel.messageLiveData.observe(viewLifecycleOwner, messageObserver)
        viewModel.detailedListLiveData.observe(viewLifecycleOwner, detailedListObserver)
        viewModel.goBackLiveData.observe(this, goBackObserver)
        viewModel.goToCartLiveData.observe(this, goToCartObserver)
        viewModel.addToFavouriteLiveData.observe(this, addToFavouriteObserver)
        chosenCapacityPositionLivedata.observe(viewLifecycleOwner, chosenCapacityPositionObserver)
        chosenColorPositionLivedata.observe(viewLifecycleOwner, chosenColorPositionObserver)
        addToCartLivedata.observe(this, addToCartObserver)
    }

    private fun setData(position: Int) = with(products!![position]) {
        binding.productTitle.text = this.title
        binding.rating.rating = this.rating
        binding.favBtn.isChecked = this.isFavorites
        pager(position)

    }

    private fun pager(position: Int) {

        shopPage.setProduct(products!![position])
        detailsPage.setProduct(products!![position])
        featuresPage.setProduct(products!![position])
        pagerAdapter.setList(listOf(shopPage, detailsPage, featuresPage))

        binding.detailViewpager.isUserInputEnabled = false
        val title = arrayOf("Shop", "Details", "Features")
        TabLayoutMediator(binding.detailTab, binding.detailViewpager) { tab, Position ->
            tab.text = title[Position]
        }.attach()

    }

    private fun adapters() {
        binding.carousel.adapter = productAdapter
        binding.carousel.apply {
            set3DItem(false)
            setAlpha(false)
            setIsScrollingEnabled(true)
            setInfinite(false)
            setIntervalRatio(0.75f)
        }
        productAdapter.submitList(products)

        binding.carousel.setItemSelectListener(object : CarouselLayoutManager.OnSelected {
                override fun onItemSelected(position: Int) {
                    currentPosition = position
                    setData(position)
                }
            })

    }

    private fun clicks() {
        productAdapter.selectListenerSet { _, position ->
//            Toast.makeText(requireContext(), productData.title, Toast.LENGTH_SHORT).show()
            binding.carousel.smoothScrollToPosition(position)
        }

        binding.back.setOnClickListener {
            viewModel.goToBack()
        }

        binding.myCart.setOnClickListener {
            viewModel.goToCart()
        }

        binding.favBtn.setOnClickListener {
            viewModel.addToFavourite(products!![currentPosition])
        }

    }

    private val progressObserver = Observer<Boolean>{

    }
    private val messageObserver = Observer<String>{
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }
    private val detailedListObserver = Observer<List<ProductData>>{
        products = it
        productAdapter.submitList(products)
        setData(currentPosition)
    }
    private val goBackObserver = Observer<Unit>{
        findNavController().popBackStack()
//        EventBus.getDefault().post(MyEventBus.MoveToExplorerEvent(R.id.explorer))
    }
    private val goToCartObserver = Observer<Unit>{
        lifecycleScope.launch {
            goToCartLivedata.value = true
        }
        findNavController().popBackStack()
    }
    private val addToCartObserver = Observer<Boolean>{
        if (it){
            products!![currentPosition].apply {
                val chosenItem = this.copy(
                    color = listOf(this.color[chosenColorPosition]),
                    capacity = listOf(this.capacity[chosenCapacityPosition]))
                viewModel.addToCart(chosenItem)
            }
            Toast.makeText(requireContext(), "Item is added to cart", Toast.LENGTH_SHORT).show()
            EventBus.getDefault().post(MyEventBus.CartBadgeRefresh(BadgeData(badgeState[1].count+1)))
            addToCartLivedata.value = false // FALSE
        }
    }
    private val addToFavouriteObserver = Observer<ProductData>{ item ->
        products!![currentPosition].isFavorites = item.isFavorites
        binding.favBtn.isChecked = item.isFavorites
        if(item.isFavorites) Toast.makeText(requireContext(), "${products!![currentPosition].title} is added to favourite", Toast.LENGTH_SHORT).show()
        else Toast.makeText(requireContext(), "${products!![currentPosition].title} is removed from favourite", Toast.LENGTH_SHORT).show()
    }
    private val chosenColorPositionObserver = Observer<Int>{
        chosenColorPosition = it
    }
    private val chosenCapacityPositionObserver = Observer<Int>{
        chosenCapacityPosition = it
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        products = listOf(
            ProductData(
                "",
                listOf("http://www.wholesalegang.com/assets/imgs/noproduct.png"),
                listOf("#FFFFFF"),
                "",
                0,
                0f,
                "",
                false,
                "",
                "",
                "_____________",
                listOf()))
    }

}