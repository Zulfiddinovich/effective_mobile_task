package ru.effectivemobile.core.presentation.viewmodel

import androidx.lifecycle.LiveData
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 12:10
   Project: EffectiveMobile Task
*/
interface CartViewModel {
    val progressLiveData: LiveData<Boolean>
    val messageLiveData: LiveData<String>
    val cartListLiveData: LiveData<CartData>

    val goBackLiveData: LiveData<Boolean>
    val checkOutLiveData: LiveData<List<BasketItemData>>

    fun getCartList()

    fun goBack()

    fun goBackCompleted()

    fun checkOut(list: List<BasketItemData>)

    fun increment(increment: Pair<BasketItemData, Int>)

    fun decrement(decrement: Pair<BasketItemData, Int>)

    fun deleteItem(delete: Pair<BasketItemData, Int>)
}