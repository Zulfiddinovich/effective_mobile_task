package ru.effectivemobile.core.presentation.viewmodel.impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData
import ru.effectivemobile.domain.usecase.CartUseCase
import ru.effectivemobile.core.presentation.viewmodel.CartViewModel
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@HiltViewModel
class CartViewModelImpl @Inject constructor(private val useCase: CartUseCase): ViewModel(), CartViewModel {
    override val progressLiveData = MutableLiveData<Boolean>()
    override val messageLiveData = MutableLiveData<String>()
    override val cartListLiveData = MutableLiveData<CartData>()
    override val goBackLiveData = MutableLiveData<Boolean>()
    override val checkOutLiveData = MutableLiveData<List<BasketItemData>>()
    private val scope = CoroutineScope(Dispatchers.Main) // todo: I use replace fragment in Host, that`s why I am using CoroutineScope(Dispatchers.Main) to avoid scope.cancel() called

    init {
        getCartList()
    }

    override fun getCartList() {
//        if (!isConnected()){
//            return
//        }

        progressLiveData.value = true
        useCase.getCartList().onEach {
            it.onSuccess {
                cartListLiveData.value = it
            }
            it.onFailure {
                messageLiveData.value = it.message
            }
            progressLiveData.value = false
        }.launchIn(scope)
    }

    override fun goBack() {
        goBackLiveData.value = true
    }

    override fun goBackCompleted() {
        goBackLiveData.value = false
    }

    override fun checkOut(list: List<BasketItemData>) {
        checkOutLiveData.value = list
    }

    override fun increment(increment: Pair<BasketItemData, Int>) {
        useCase.increment(increment)
        getCartList()
    }

    override fun decrement(decrement: Pair<BasketItemData, Int>) {
        useCase.decrement(decrement)
        getCartList()
    }

    override fun deleteItem(delete: Pair<BasketItemData, Int>) {
        useCase.deleteItem(delete)
        getCartList()
    }

}