package ru.effectivemobile.presentation.ui.screen

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import ru.effectivemobile.data.model.common.BadgeData
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData
import ru.effectivemobile.data.model.common.MyEventBus
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FragmentCartBinding
import ru.effectivemobile.presentation.ui.adapter.CartListAdapter
import ru.effectivemobile.core.presentation.viewmodel.CartViewModel
import ru.effectivemobile.core.presentation.viewmodel.impl.CartViewModelImpl

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class CartFragment : Fragment(R.layout.fragment_cart) {
    private val binding by viewBinding(FragmentCartBinding::bind)
    private val viewModel: CartViewModel by viewModels<CartViewModelImpl>()
    private val cartAdapter = CartListAdapter()
    private var cartItemList: CartData? = null

    private val mTag = "CartFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        liveDatas()
        clicks()
        adapter()
        setData()

    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun liveDatas() {
        viewModel.progressLiveData.observe(viewLifecycleOwner, progressObserver)
        viewModel.messageLiveData.observe(viewLifecycleOwner, messageObserver)
        viewModel.cartListLiveData.observe(viewLifecycleOwner, cartListObserver)
        viewModel.goBackLiveData.observe(this, goBackObserver)
        viewModel.checkOutLiveData.observe(this, checkOutObserver)
    }

    private fun setData() {
        binding.totalSum.text = cartItemList?.total.toString() + " us"
        binding.deliveryCost.text = cartItemList?.delivery
        val count = if (cartItemList == null) { 0 }
                    else { cartItemList!!.basket.size}
        EventBus.getDefault().post(MyEventBus.CartBadgeRefresh(BadgeData(count)))
    }

    private fun adapter() {
        binding.cartRecycle.adapter = cartAdapter
        binding.cartRecycle.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        cartAdapter.submitList(cartItemList?.basket)
        if(cartItemList != null) {
            binding.lottie.isVisible = cartItemList!!.basket.isEmpty()
        }

    }

    private fun clicks() {
        binding.cartCheckoutBtn.setOnClickListener {
            cartItemList?.let {
                viewModel.checkOut(it.basket)
            }
        }

        binding.back.setOnClickListener {
            viewModel.goBack()
        }

        cartAdapter.incrementListenerSet { basketItem, position ->
            Log.d(mTag, "increment: 1")
            viewModel.increment(Pair(basketItem, position))
        }

        cartAdapter.decrementListenerSet { basketItem, position ->
            viewModel.decrement(Pair(basketItem, position))
        }

        cartAdapter.deleteListenerSet { basketItem, position ->
            viewModel.deleteItem(Pair(basketItem, position))
        }

        cartAdapter.refreshListenerSet {
            setTotalSumValue()
        }
    }

    private fun setTotalSumValue() {
        var totalSum = 0
        if (cartAdapter.currentList.isNotEmpty()) {
            cartAdapter.currentList.onEach {
                totalSum += (it.price * it.counter)
            }
        }
        cartItemList?.total = totalSum
        setData()
    }

    private val progressObserver = Observer<Boolean>{

    }
    private val messageObserver = Observer<String>{
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }
    private val cartListObserver = Observer<CartData>{
        cartItemList = it
        cartAdapter.submitList(cartItemList?.basket)
        binding.lottie.isVisible = cartItemList!!.basket.isEmpty()
    }
    private val goBackObserver = Observer<Boolean>{
        if (it) {
            EventBus.getDefault().post(MyEventBus.MoveToExplorerEvent(R.id.explorer))
            viewModel.goBackCompleted()
        }
    }
    private val checkOutObserver = Observer<List<BasketItemData>>{
        Toast.makeText(requireContext(),"CheckOut is processing for ${it.size} items", Toast.LENGTH_SHORT).show()
    }


}