package ru.effectivemobile.presentation.ui.screen

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import ru.effectivemobile.core.presentation.viewmodel.MainViewModel
import ru.effectivemobile.core.presentation.viewmodel.impl.MainViewModelImpl
import ru.effectivemobile.data.model.common.*
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FragmentHomeBinding
import ru.effectivemobile.presentation.ui.adapter.BestSellerAdapter
import ru.effectivemobile.presentation.ui.adapter.delegates.CategoryListAdapter
import ru.effectivemobile.presentation.ui.adapter.SalesAdapter
import ru.effectivemobile.presentation.ui.dialog.FilterDialog
import ru.effectivemobile.presentation.util.hideKeyBoard

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home) {
    private val binding by viewBinding(FragmentHomeBinding::bind)
    private val viewModel: MainViewModel by viewModels<MainViewModelImpl>()
    private val categoryAdapter by lazy { CategoryListAdapter(categoryAdapterListener) }
    private val salesAdapter by lazy { SalesAdapter() }
    private val bestSellerAdapter by lazy { BestSellerAdapter() }
    private val filterDialog by lazy { FilterDialog(requireContext()) }

    private val categoryList: List<CategoryParent> = listOf()


    private var address = ""
    private val addressList = arrayOf("Ostapovskiy Proyezd, 5, Moscow", "Nikoloyamskiy Pereulok, 2, Moscow")

    private val addressAdapter by lazy {
        ArrayAdapter(requireContext(), R.layout.address_spinner_item, addressList).apply {
            setDropDownViewResource(R.layout.address_spinner_item)
        }
    }

    private val salesList: List<SalesData> = listOf()
    private val bestSellerList: List<BestSellerData> = listOf()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapters()
        liveDatas()
        clicks()
        spinners()
    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun liveDatas() {
        viewModel.progressLiveData.observe(viewLifecycleOwner, progressObserver)
        viewModel.messageLiveData.observe(viewLifecycleOwner, messageObserver)
        viewModel.categoryListLiveData.observe(viewLifecycleOwner, categoryListObserver)
        viewModel.saleListLiveData.observe(viewLifecycleOwner, saleListObserver)
        viewModel.bestSellerListLiveData.observe(viewLifecycleOwner, bestSellerListObserver)

        viewModel.startFilteringLiveData.observe(this, startFilteringObserver)
        viewModel.selectCategoryLiveData.observe(viewLifecycleOwner, selectCategoryObserver)
        viewModel.searchByQueryLiveData.observe(viewLifecycleOwner, searchByQueryObserver)
        viewModel.buyItNowLiveData.observe(this, buyItNowObserver)
        viewModel.bestSellerItemLiveData.observe(this, bestSellerItemObserver)
        viewModel.changeFavouriteItemLiveData.observe(this, changeFavouriteItemObserver)
    }

    private fun spinners() {

        (binding.addressSpinner.getChildAt(0) as Spinner).apply {
            onItemSelectedListener = addressListener
            adapter = addressAdapter
        }

    }


    private fun clicks() {
        binding.salesLayout.seeMore.setOnClickListener {
            viewModel.seeMoreHotSales()
        }

        binding.application.setOnClickListener {
            hideKeyBoard()
            viewModel.searchByQuery(binding.queryEditText.text.toString())
        }

        binding.filterBtn.setOnClickListener {
            viewModel.startFiltering()
        }

        salesAdapter.butNowListenerSet {
            viewModel.buyNowItem(it)
        }

        bestSellerAdapter.selectListenerSet {
            viewModel.bestSellerItem(it)
        }
        bestSellerAdapter.favListenerSet { item ->
            viewModel.changeFavouriteItem(item)
        }

    }

    private fun adapters() {
        binding.categoryLayout.categoryRV.adapter = categoryAdapter
        binding.categoryLayout.categoryRV.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        categoryAdapter.items = categoryList


        val snap = LinearSnapHelper()
        binding.salesLayout.salesRV.onFlingListener = null
        snap.attachToRecyclerView(binding.salesLayout.salesRV)
        binding.salesLayout.salesRV.adapter = salesAdapter
        binding.salesLayout.salesRV.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        salesAdapter.submitList(salesList)

        binding.bestsellerLayout.salesRV.adapter = bestSellerAdapter
        binding.bestsellerLayout.salesRV.layoutManager =
            GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        bestSellerAdapter.submitList(bestSellerList)
    }


    private val addressListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            address = addressList[position]
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {

        }
    }

    private val progressObserver = Observer<Boolean>{

    }
    private val messageObserver = Observer<String>{
        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
    }
    private val categoryListObserver = Observer<List<CategoryParent>>{
        categoryAdapter.items = (it)
    }
    private val saleListObserver = Observer<List<SalesData>>{
        salesAdapter.submitList(it)
    }
    private val bestSellerListObserver = Observer<List<BestSellerData>>{
        bestSellerAdapter.submitList(it)
    }
    private val startFilteringObserver = Observer<Unit>{
        filterDialog.bottomSheet.show()
    }
    private val selectCategoryObserver = Observer<CategoryParent>{ item ->

        categoryClicked(item)
    }
    private val searchByQueryObserver = Observer<String>{
        Toast.makeText(requireContext(),"Search by \"$it\"", Toast.LENGTH_SHORT).show()
    }
    private val buyItNowObserver = Observer<SalesData>{
        EventBus.getDefault().post(MyEventBus.BestSellerClickEvent())
    }
    private val bestSellerItemObserver = Observer<BestSellerData>{
        EventBus.getDefault().post(MyEventBus.BestSellerClickEvent())
    }
    private val changeFavouriteItemObserver = Observer<BestSellerData>{ item ->
        bestSellerAdapter.currentList.onEachIndexed { index, it ->
            if (it.title == item.title) {
                it.is_favorites = !item.is_favorites
                if(item.is_favorites) Toast.makeText(requireContext(), "${bestSellerAdapter.currentList[index].title} is added to favourite", Toast.LENGTH_SHORT).show()
                else Toast.makeText(requireContext(), "${bestSellerAdapter.currentList[index].title} is removed from favourite", Toast.LENGTH_SHORT).show()
//                bestSellerAdapter.notifyItemChanged(index)
            }
        }
    }

    private val categoryAdapterListener = object: (CategoryParent) -> Unit {
        override fun invoke(item: CategoryParent) {
            viewModel.selectedCategory(item)
        }
    }

    private fun categoryClicked(item: CategoryParent) {
        val title = if (item is CategoryData1) item.title
                else if (item is CategoryData2) item.title else ""
        Toast.makeText(requireContext(), title + " category is selected", Toast.LENGTH_SHORT).show()

        categoryAdapter.items.onEach {
            if (it is CategoryData1) {
                it.isSelected = false
                if (it.title == title) {
                    it.isSelected = true
                }
            }
            if (it is CategoryData2) {
                it.isSelected = false
                if (it.title == title) {
                    it.isSelected = true
                }
            }
        }
        categoryAdapter.notifyDataSetChanged()
    }


}
