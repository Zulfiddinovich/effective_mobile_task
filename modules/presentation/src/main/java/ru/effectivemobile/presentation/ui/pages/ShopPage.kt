package ru.effectivemobile.presentation.ui.pages

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.core.view.setMargins
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.presentation.util.Constants
import ru.effectivemobile.presentation.util.Constants.Companion.chosenCapacityPositionLivedata
import ru.effectivemobile.presentation.util.Constants.Companion.chosenColorPositionLivedata
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.ShopPageBinding
import ru.effectivemobile.presentation.util.px

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class ShopPage : Fragment(R.layout.shop_page) {
    private val binding by viewBinding(ShopPageBinding::bind)
    private lateinit var productData: ProductData
    private val mTag = "ShopPage"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    }

    fun setProduct(data: ProductData){
        CoroutineScope(Dispatchers.Main).launch {
            productData = data
            setData()
        }

    }

    private fun setData() {
        binding.cpuTextView.text = productData.CPU

        binding.cameraTextView.text = productData.camera

        binding.ramTextView.text = productData.ssd

        binding.romTextView.text = productData.sd

        setColors()
        setCapacity()

        binding.addCartBtn.text = requireActivity().getString(R.string.add_to_cart) + "   $${productData.price}"

        binding.addCartBtn.setOnClickListener {
            Constants.addToCartLivedata.value = true
        }

        binding.colorOptions.setOnCheckedChangeListener { _, checkedId ->
            chosenColorPositionLivedata.value = checkedId
        }
        binding.capacityOptions.setOnCheckedChangeListener { _, checkedId ->
            binding.capacityOptions.children.onEachIndexed { index, view ->
                if(view.id == checkedId) {
                    chosenCapacityPositionLivedata.value = index
                    return@onEachIndexed
                }
            }
        }

    }

    private fun setColors() {
        binding.colorOptions.clearCheck()
        binding.colorOptions.removeAllViews()

        productData.color.onEachIndexed { index, _ ->
            val option = RadioButton(requireContext())
            binding.colorOptions.addView(option)
            option.apply {
                this.id = index
                this.height = 40.px
                this.width = 40.px
                this.buttonDrawable = null
                (this.layoutParams as RadioGroup.LayoutParams).setMargins(4.px)
                this.setBackgroundResource(R.drawable.bg_round_radio_button)
                this.backgroundTintList = ColorStateList.valueOf(Color.parseColor(productData.color[index]))
                this.foreground = ContextCompat.getDrawable(requireContext(), R.drawable.color_radio_btn_foreground)
            }
        }
        (binding.colorOptions.getChildAt(0) as RadioButton).isChecked = true
    }

    private fun setCapacity() {
        productData.capacity.onEachIndexed { index, string ->
            (binding.capacityOptions.getChildAt(index) as RadioButton).apply {
                visibility = View.VISIBLE
                text = string
            }
        }
    (binding.capacityOptions.getChildAt(0) as RadioButton).isChecked = true
    }
}
