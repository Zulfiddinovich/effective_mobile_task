package ru.effectivemobile.core.presentation.viewmodel.impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.effectivemobile.data.model.common.BestSellerData
import ru.effectivemobile.data.model.common.CategoryData1
import ru.effectivemobile.data.model.common.SalesData
import ru.effectivemobile.domain.usecase.MainUseCase
import ru.effectivemobile.core.presentation.viewmodel.MainViewModel
import ru.effectivemobile.data.model.common.CategoryParent
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 12:10
   Project: EffectiveMobile Task
*/
@HiltViewModel
class MainViewModelImpl @Inject constructor(private val useCase: MainUseCase): ViewModel(), MainViewModel {
    override val progressLiveData = MutableLiveData<Boolean>()
    override val messageLiveData = MutableLiveData<String>()
    override val categoryListLiveData = MutableLiveData<List<CategoryParent>>()
    override val saleListLiveData = MutableLiveData<List<SalesData>>()
    override val bestSellerListLiveData = MutableLiveData<List<BestSellerData>>()
    override val startFilteringLiveData = MutableLiveData<Unit>()
    override val selectCategoryLiveData = MutableLiveData<CategoryParent>()
    override val searchByQueryLiveData = MutableLiveData<String>()
    override val buyItNowLiveData = MutableLiveData<SalesData>()
    override val bestSellerItemLiveData = MutableLiveData<BestSellerData>()
    override val changeFavouriteItemLiveData = MutableLiveData<BestSellerData>()

    init {
        getCategoryList()
        getHotSalesList()
        getBestSellerList()
    }

    override fun getCategoryList() {
        progressLiveData.value = true
        useCase.getCategoryList().onEach {
            categoryListLiveData.value = it
            progressLiveData.value = false
        }.launchIn(viewModelScope)
    }

    override fun getHotSalesList() {
//        if (!isConnected()){
//            return
//        }
        progressLiveData.value = true
        useCase.getHotSalesList().onEach { it ->
            it.onSuccess {
                saleListLiveData.value = it
            }
            it.onFailure {
                messageLiveData.value = it.message
            }
            progressLiveData.value = false
        }.launchIn(viewModelScope)
    }

    override fun getBestSellerList() {
//        if (!isConnected()){
//            return
//        }
        progressLiveData.value = true
        useCase.getBestSellerList().onEach { it ->
            it.onSuccess {
                bestSellerListLiveData.value = it
            }
            it.onFailure {
                messageLiveData.value = it.message
            }
            progressLiveData.value = false
        }.launchIn(viewModelScope)
    }

    override fun startFiltering() {
        startFilteringLiveData.value = Unit
    }

    override fun viewAllCategory() {
        // todo
    }

    override fun selectedCategory(category: CategoryParent) {
        selectCategoryLiveData.value = category
    }

    override fun searchByQuery(query: String) {
        searchByQueryLiveData.value = query
    }

    override fun seeMoreHotSales() {
        //
    }

    override fun buyNowItem(item: SalesData) {
        buyItNowLiveData.value = item
    }

    override fun bestSellerItem(item: BestSellerData) {
        bestSellerItemLiveData.value = item
    }

    override fun seeMoreBestSeller() {
        // todo
    }

    override fun changeFavouriteItem(item: BestSellerData) {
        changeFavouriteItemLiveData.value = item
    }


}