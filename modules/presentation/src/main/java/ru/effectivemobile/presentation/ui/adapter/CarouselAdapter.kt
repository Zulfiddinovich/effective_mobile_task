package ru.effectivemobile.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.presentation.databinding.ProductPagerItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class CarouselAdapter: ListAdapter<ProductData, CarouselAdapter.CategoryViewHolder>(DifUtil) {
    private var selectListener: ((ProductData, Int) -> Unit)? = null


    object DifUtil: DiffUtil.ItemCallback<ProductData>() {
        override fun areItemsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: ProductData, newItem: ProductData): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }


    inner class CategoryViewHolder(private val binding: ProductPagerItemBinding): RecyclerView.ViewHolder(binding.root){
        init {

        }

        fun bind(){
            Glide.with(binding.image).load(getItem(absoluteAdapterPosition).images[0]).into(binding.image)

            binding.card.setOnClickListener {
                selectListener?.invoke(getItem(absoluteAdapterPosition), absoluteAdapterPosition)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
        CategoryViewHolder(ProductPagerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind()
    }

    fun selectListenerSet(listener: (ProductData, Int) -> Unit){
        this.selectListener = listener
    }


}