package ru.effectivemobile.presentation.ui.adapter.delegates

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate
import ru.effectivemobile.data.model.common.CategoryData2
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.CategoryItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 17.11.2022 at 15:55
   Project: EffectiveMobile Task
*/

class CategoryTwoAdapterDel(private val selectListener: (CategoryData2) -> Unit):
    AbsListItemAdapterDelegate<CategoryData2, CategoryParent, CategoryTwoAdapterDel.CategoryViewHolder2>() {

    inner class CategoryViewHolder2(private val binding: CategoryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CategoryData2){

            binding.categoryItem.setOnClickListener {
                Log.d("Click", "categoryAdapterDelegate, Click on $item")
                selectListener.invoke(item)
            }

            Glide.with(binding.categoryImage).load(item.image).into(binding.categoryImage)

            binding.title.text = item.title
            if (item.isSelected) {
                binding.title.setTextColor(ContextCompat.getColor(binding.root.context, R.color.orange))
                binding.categoryImage.setColorFilter(ContextCompat.getColor(binding.root.context, R.color.white))
                binding.cardView.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.orange))
                binding.cardView.cardElevation = 0f
            } else {
                binding.title.setTextColor(ContextCompat.getColor(binding.root.context, R.color.dark_blue))
                binding.categoryImage.setColorFilter(ContextCompat.getColor(binding.root.context, R.color.grey))
                binding.cardView.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.white))
                binding.cardView.cardElevation = 20f
            }
        }
    }

    public override fun onCreateViewHolder(parent: ViewGroup): CategoryViewHolder2 =
        CategoryViewHolder2(CategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun isForViewType(item: CategoryParent, items: MutableList<CategoryParent>, position: Int): Boolean {
        return items[position] is CategoryData2
    }

    override fun onBindViewHolder(
        item: CategoryData2,
        holder: CategoryViewHolder2,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }
}


fun a(){}