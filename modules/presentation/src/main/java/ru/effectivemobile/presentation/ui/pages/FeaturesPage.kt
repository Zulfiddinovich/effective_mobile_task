package ru.effectivemobile.presentation.ui.pages

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.FeaturesPageBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class FeaturesPage : Fragment(R.layout.features_page) {
    private val binding by viewBinding(FeaturesPageBinding::bind)
    private lateinit var productData: ProductData


    fun setProduct(data: ProductData){
        this.productData = data
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.root.setOnClickListener {  }
    }
}