package ru.effectivemobile.presentation.viewmodel

import androidx.lifecycle.LiveData

/*
   Author: Zukhriddin Kamolov
   Created: 18.11.2022 at 9:44
   Project: EffectiveMobile Task
*/

interface SplashViewModel {
    val goMainScreenLiveData: LiveData<Unit>

    fun goMainScreen()

}