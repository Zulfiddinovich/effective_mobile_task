package ru.effectivemobile.core.presentation.viewmodel

import androidx.lifecycle.LiveData
import ru.effectivemobile.data.model.common.ProductData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 12:10
   Project: EffectiveMobile Task
*/
interface DetailedProductViewModel {

    val progressLiveData: LiveData<Boolean>
    val messageLiveData: LiveData<String>
    val detailedListLiveData: LiveData<List<ProductData>>

    val goBackLiveData: LiveData<Unit>
    val goToCartLiveData: LiveData<Unit>
    val addToCartLiveData: LiveData<Boolean>
    val addToFavouriteLiveData: LiveData<ProductData>

    fun getDetailedList()

    fun goToBack()

    fun goToCart()

    fun addToCart(item: ProductData)

    fun addToFavourite(item: ProductData)
}