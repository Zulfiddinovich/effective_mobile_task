package ru.effectivemobile.presentation.ui.adapter.delegates

import android.util.Log
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import ru.effectivemobile.data.model.common.CategoryData1
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.presentation.R
import ru.effectivemobile.presentation.databinding.CategoryItemBinding

/*
   Author: Zukhriddin Kamolov
   Created: 17.11.2022 at 16:28
   Project: EffectiveMobile Task
*/

fun categoryOneAdapterDel(selectListener: (CategoryData1) -> Unit) = adapterDelegateViewBinding<CategoryData1, CategoryParent, CategoryItemBinding>(
    { layoutInflater, root -> CategoryItemBinding.inflate(layoutInflater, root, false) }
) {
    binding.categoryItem.setOnClickListener {
        selectListener.invoke(item)
    }
    bind {
        Glide.with(binding.categoryImage).load(item.image).into(binding.categoryImage)

        binding.title.text = item.title
        if (item.isSelected) {
            binding.title.setTextColor(ContextCompat.getColor(binding.root.context, R.color.orange))
            binding.categoryImage.setColorFilter(ContextCompat.getColor(binding.root.context, R.color.white))
            binding.cardView.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.orange))
            binding.cardView.cardElevation = 0f
        } else {
            binding.title.setTextColor(ContextCompat.getColor(binding.root.context, R.color.dark_blue))
            binding.categoryImage.setColorFilter(ContextCompat.getColor(binding.root.context, R.color.grey))
            binding.cardView.setCardBackgroundColor(ContextCompat.getColor(binding.root.context, R.color.white))
            binding.cardView.cardElevation = 20f
        }
    }
}
