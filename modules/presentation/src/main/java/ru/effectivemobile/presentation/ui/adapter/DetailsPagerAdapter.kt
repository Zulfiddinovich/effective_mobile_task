package ru.effectivemobile.presentation.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class DetailsPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle):
    FragmentStateAdapter(fragmentManager,lifecycle) {
    private var mList: List<Fragment> = ArrayList()

    override fun getItemCount(): Int = mList.size

    override fun createFragment(position: Int): Fragment = mList[position]

    fun setList(list: List<Fragment>){
        this.mList = list
    }
}