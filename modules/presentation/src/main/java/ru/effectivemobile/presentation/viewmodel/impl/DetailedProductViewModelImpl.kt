package ru.effectivemobile.core.presentation.viewmodel.impl

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.domain.usecase.DetailedProductUseCase
import ru.effectivemobile.core.presentation.viewmodel.DetailedProductViewModel
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 12:10
   Project: EffectiveMobile Task
*/
@HiltViewModel
class DetailedProductViewModelImpl @Inject constructor(private val useCase: DetailedProductUseCase): ViewModel(), DetailedProductViewModel {
    override val progressLiveData = MutableLiveData<Boolean>()
    override val messageLiveData = MutableLiveData<String>()
    override val detailedListLiveData = MutableLiveData<List<ProductData>>()
    override val goBackLiveData = MutableLiveData<Unit>()
    override val goToCartLiveData = MutableLiveData<Unit>()
    override val addToCartLiveData = MutableLiveData<Boolean>()
    override val addToFavouriteLiveData = MutableLiveData<ProductData>()

    init {
        getDetailedList()
    }


    override fun getDetailedList() {
//        if (!isConnected()){
//            return
//        }
        progressLiveData.value = true
        useCase.getDetailedList().onEach {
            it.onSuccess {
                detailedListLiveData.value = it
            }
            it.onFailure {
                messageLiveData.value = it.message
            }
            progressLiveData.value = false
        }.launchIn(viewModelScope)
    }

    override fun goToBack() {
        goBackLiveData.value = Unit
    }

    override fun goToCart() {
        goToCartLiveData.value = Unit
    }

    override fun addToCart(item: ProductData) {
        // todo
    }

    override fun addToFavourite(item: ProductData) {
        item.isFavorites = !item.isFavorites
        addToFavouriteLiveData.value = item
        // todo
    }



}