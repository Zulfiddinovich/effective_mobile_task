package ru.effectivemobile.presentation.ui.adapter.delegates

import android.app.Activity
import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import ru.effectivemobile.data.model.common.CategoryParent

/*
   Author: Zukhriddin Kamolov
   Created: 17.11.2022 at 16:51
   Project: EffectiveMobile Task
*/

class CategoryListAdapter(selectListener: (CategoryParent) -> Unit):
    AsyncListDifferDelegationAdapter<CategoryParent>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<CategoryParent> =
            object : DiffUtil.ItemCallback<CategoryParent>() {
                override fun areItemsTheSame(oldItem: CategoryParent, newItem: CategoryParent): Boolean {
                    return oldItem.getItemImage() == newItem.getItemImage()
                }

                override fun areContentsTheSame(oldItem: CategoryParent, newItem: CategoryParent): Boolean {
                    return oldItem.getItemTitle() == newItem.getItemTitle()
                }
            }
    }

    init {
        delegatesManager
            .addDelegate(categoryOneAdapterDel(selectListener))
            .addDelegate(CategoryTwoAdapterDel(selectListener))
    }
}