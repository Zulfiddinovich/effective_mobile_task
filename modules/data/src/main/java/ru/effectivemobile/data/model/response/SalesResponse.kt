package ru.effectivemobile.data.model.response

import ru.effectivemobile.data.model.common.SalesData

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

data class SalesResponse(
    val id: Int,
    val is_new: Boolean,
    val title: String,
    val subtitle: String,
    val picture: String,
    val is_buy: Boolean
) {
    fun toSalesData() = SalesData(
        id, is_new, title, subtitle, picture, is_buy
    )
}