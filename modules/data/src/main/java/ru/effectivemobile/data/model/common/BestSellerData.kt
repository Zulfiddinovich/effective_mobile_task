package ru.effectivemobile.data.model.common

import ru.effectivemobile.data.model.response.BestSellerResponse

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 10:11
   Project: EffectiveMobile Task
*/

data class BestSellerData(
	var is_favorites: Boolean,
	val discount_price: Int,
	val id: Int,
	val title: String,
	val price_without_discount: Int,
	val picture: String
){
	fun toBestSellerResponse() = BestSellerResponse(
		is_favorites, discount_price, id, title, price_without_discount, picture
	)
}