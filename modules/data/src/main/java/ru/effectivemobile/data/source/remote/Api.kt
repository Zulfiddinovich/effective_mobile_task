package ru.effectivemobile.data.source.remote

import retrofit2.Response
import retrofit2.http.GET
import ru.effectivemobile.data.model.response.BestSellerResponse
import ru.effectivemobile.data.model.response.CartResponse
import ru.effectivemobile.data.model.response.ProductDetailResponse
import ru.effectivemobile.data.model.response.SalesResponse

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

interface Api {

    @GET("/v3/3941f34f-936e-4261-b7a7-323f732b4e8f/")
    suspend fun getHotSalesList(): Response<List<SalesResponse>>

    @GET("/v3/4b312182-1545-420f-8b7c-fbc82599d154/")
    suspend fun getBestSellerList(): Response<List<BestSellerResponse>>

    @GET("/v3/6a48f30a-68e4-43ad-960e-744c44f06e23/")
    suspend fun getDetailedList(): Response<List<ProductDetailResponse>>

    @GET("/v3/7283bfea-c537-491f-a73f-36965fe9fe98/")
    suspend fun getCartList(): Response<CartResponse>

}