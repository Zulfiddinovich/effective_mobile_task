package ru.effectivemobile.data.model.common

import ru.effectivemobile.data.model.response.BasketItemResponse
import ru.effectivemobile.data.model.response.CartResponse

/*
   Author: Zukhriddin Kamolov
   Created: 10.11.2022 at 18:06
   Project: EffectiveMobile Task
*/

data class CartData(
    val id: String,
    var total: Int,
    val delivery: String,
    val basket: ArrayList<BasketItemData>
){
    fun toCartResponse() = CartResponse(
        id, total, delivery, basket.map { it.toBasketItemResponse() } as ArrayList
    )
}



data class BasketItemData(
    val id: Int,
    val title: String,
    val images: String,
    val price: Int,
    var counter: Int = 1
){
    fun toBasketItemResponse() = BasketItemResponse(
        id, title, images, price, counter
    )
}