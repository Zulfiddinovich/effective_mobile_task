package ru.effectivemobile.data.model.common

import ru.effectivemobile.data.model.response.ProductDetailResponse

/*
   Author: Zukhriddin Kamolov
   Created: 08.11.2022 at 6:09
   Project: EffectiveMobile Task
*/

data class ProductData(
	var sd: String,
	var images: List<String>,
	var color: List<String>,
	var ssd: String,
	var price: Int,
	var rating: Float,
	var CPU: String,
	var isFavorites: Boolean,
	var id: String,
	var camera: String,
	var title: String,
	var capacity: List<String>
){
	fun toProductDetailResponse() = ProductDetailResponse(
		sd, images, color, ssd, price, rating, CPU, isFavorites, id, camera, title, capacity
	)
}
