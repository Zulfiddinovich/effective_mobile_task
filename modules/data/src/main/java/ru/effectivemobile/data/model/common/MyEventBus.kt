package ru.effectivemobile.data.model.common

/*
   Author: Zukhriddin Kamolov
   Created: 11.11.2022 at 12:20
   Project: EffectiveMobile Task
*/

sealed class MyEventBus{
    class SeeAllCategoryEvent(): MyEventBus()
    class SeeHotSalesEvent(): MyEventBus()
    class BuyNowEvent(): MyEventBus()
    class SeeBestSellersEvent(): MyEventBus()
    class BestSellerClickEvent(): MyEventBus()

    class MoveToCartEvent(val fragmentIdMenu: Int): MyEventBus()
    class MoveToExplorerEvent(val fragmentIdMenu: Int): MyEventBus()

    class CartBadgeRefresh(val data: BadgeData): MyEventBus()

}