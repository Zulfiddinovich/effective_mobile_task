package ru.effectivemobile.data.model.common

import ru.effectivemobile.data.model.response.SalesResponse

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

data class SalesData(
    val id: Int,
    val is_new: Boolean? = false,
    val title: String,
    val subtitle: String,
    val picture: String,
    val is_buy: Boolean
){
    fun toSalesResponse() = SalesResponse(
        id, is_new ?: false, title, subtitle, picture, is_buy
    )
}