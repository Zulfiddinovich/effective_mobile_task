package ru.effectivemobile.data.model.response

import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData

/*
   Author: Zukhriddin Kamolov
   Created: 10.11.2022 at 18:06
   Project: EffectiveMobile Task
*/

data class CartResponse(
    val id: String,
    var total: Int,
    val delivery: String,
    val basket: ArrayList<BasketItemResponse>
){
    fun toCartData() = CartData(
        id, total, delivery, basket.map { it.toBasketItemData() } as ArrayList
    )
}

data class BasketItemResponse(
    val id: Int,
    val title: String,
    val images: String,
    val price: Int,
    var counter: Int = 1
){
    fun toBasketItemData() = BasketItemData(
        id, title, images, price, counter
    )
}