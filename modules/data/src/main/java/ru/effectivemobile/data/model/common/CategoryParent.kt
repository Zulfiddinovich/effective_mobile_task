package ru.effectivemobile.data.model.common

/*
   Author: Zukhriddin Kamolov
   Created: 17.11.2022 at 16:07
   Project: EffectiveMobile Task
*/

interface CategoryParent {

    fun getItemImage(): Int

    fun getItemTitle(): String
}