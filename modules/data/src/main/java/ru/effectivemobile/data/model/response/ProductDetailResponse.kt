package ru.effectivemobile.data.model.response

import ru.effectivemobile.data.model.common.ProductData

/*
   Author: Zukhriddin Kamolov
   Created: 08.11.2022 at 6:09
   Project: EffectiveMobile Task
*/

data class ProductDetailResponse(
	val sd: String,
	val images: List<String>,
	val color: List<String>,
	val ssd: String,
	val price: Int,
	val rating: Float,
	val CPU: String,
	val isFavorites: Boolean,
	val id: String,
	val camera: String,
	val title: String,
	val capacity: List<String>
){
	fun toProductData() = ProductData(
		sd, images, color, ssd, price, rating, CPU, isFavorites, id, camera, title, capacity
	)
}
