package ru.effectivemobile.data.model.common

import androidx.annotation.DrawableRes

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 10:11
   Project: EffectiveMobile Task
*/

data class CategoryData2(
    @DrawableRes val image: Int,
    val title: String,
    var isSelected: Boolean,
): CategoryParent {
    override fun getItemImage(): Int{
        return image
    }

    override fun getItemTitle(): String {
        return title
    }
}