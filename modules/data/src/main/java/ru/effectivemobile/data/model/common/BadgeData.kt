package ru.effectivemobile.data.model.common

import androidx.annotation.DrawableRes

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 13:58
   Project: EffectiveMobile Task
*/

data class BadgeData(var count: Int, val resId: Int = 0) {
    var isVisible: Boolean = false
    init {
        isVisible = count != 0
    }

}