package ru.effectivemobile.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import ru.effectivemobile.data.model.common.ProductData
import ru.effectivemobile.domain.repository.MainRepository
import ru.effectivemobile.domain.usecase.DetailedProductUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class DetailedProductUseCaseImpl @Inject constructor(private val repository: MainRepository):
    DetailedProductUseCase {


    override fun getDetailedList() = flow<Result<List<ProductData>>> {
        repository.getDetailedList().collect {
            it.onSuccess {
                val response = it.map { it.toProductData() }
                emit(Result.success(response))
            }
            it.onFailure {
                emit(Result.failure(it))
            }
        }
    }.flowOn(Dispatchers.IO)

}