package ru.effectivemobile.domain.repository.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import ru.effectivemobile.data.model.common.CategoryData2
import ru.effectivemobile.data.model.common.CategoryData1
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.data.model.response.*
import ru.effectivemobile.data.source.remote.Api
import ru.effectivemobile.domain.R
import ru.effectivemobile.domain.repository.MainRepository
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class MainRepositoryImpl @Inject constructor(private val api: Api): MainRepository {
    private val TAG = "MainRepositoryImpl"
    private var cartResponse: CartResponse? = null

    override fun getCategoryList() = flow<List<CategoryParent>> {
        val categoryList: List<CategoryParent> = listOf(
            CategoryData1(R.drawable.phone, "Phones", true),
            CategoryData2(R.drawable.computer, "Computer", false),
            CategoryData1(R.drawable.health, "Health", false),
            CategoryData2(R.drawable.books, "Books", false),
            CategoryData1(R.drawable.tools, "Tools", false)
        )
        emit(categoryList)
    }


    override fun getHotSalesList() = flow<Result<List<SalesResponse>>> {
        val response = api.getHotSalesList()
        if (response.isSuccessful){
            response.body()?.let {
                emit(Result.success(it))
            }
        } else {
            emit(Result.failure(Throwable("Result unsuccessful")))
        }
    }.catch {
        emit(Result.failure(Throwable("Error: ${it.message}")))
    }.flowOn(Dispatchers.IO)


    override fun getBestSellerList() = flow<Result<List<BestSellerResponse>>> {
        val response = api.getBestSellerList()
        if (response.isSuccessful){
            response.body()?.let {
                emit(Result.success(it))
            }
        } else {
            emit(Result.failure(Throwable("Result unsuccessful")))
        }
    }.catch {
        emit(Result.failure(Throwable("Error: ${it.message}")))
    }.flowOn(Dispatchers.IO)


    override fun getDetailedList() = flow<Result<List<ProductDetailResponse>>> {
        val response = api.getDetailedList()
        if (response.isSuccessful){
            response.body()?.let {
                emit(Result.success(it))
            }
        } else {
            emit(Result.failure(Throwable("Result unsuccessful")))
        }
    }.catch {
        emit(Result.failure(Throwable("Error: ${it.message}")))
    }.flowOn(Dispatchers.IO)


    override fun getCartList() = flow<Result<CartResponse>> {
        val response = api.getCartList()
        if (response.isSuccessful){
            response.body()?.let {
                if (cartResponse == null) {
                    it.basket.onEach { basket ->
                        basket.counter = 1
                    }
                    cartResponse = it
                }

                emit(Result.success(cartResponse!!))
            }
        } else {
            emit(Result.failure(Throwable("Result unsuccessful")))
        }
    }.catch {
        emit(Result.failure(Throwable("Error: ${it.message}")))
    }.flowOn(Dispatchers.IO)


    override fun increment(increment: Pair<BasketItemResponse, Int>) {
        cartResponse!!.basket[increment.second].counter = increment.first.counter+1
    }


    override fun decrement(decrement: Pair<BasketItemResponse, Int>) {
        if (cartResponse!!.basket.size > 0 && cartResponse!!.basket[decrement.second].counter >= 1)
            cartResponse!!.basket[decrement.second].counter = decrement.first.counter-1
    }


    override fun deleteItem(delete: Pair<BasketItemResponse, Int>) {
        if (cartResponse!!.basket.size > 0 && cartResponse!!.basket.size > delete.second) cartResponse!!.basket.removeAt(delete.second)
    }


}