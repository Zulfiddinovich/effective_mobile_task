package ru.effectivemobile.domain.usecase

import kotlinx.coroutines.flow.Flow
import ru.effectivemobile.data.model.common.ProductData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 11:10
   Project: EffectiveMobile Task
*/

interface DetailedProductUseCase {

    fun getDetailedList(): Flow<Result<List<ProductData>>>

}