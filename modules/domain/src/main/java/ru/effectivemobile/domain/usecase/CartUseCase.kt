package ru.effectivemobile.domain.usecase

import kotlinx.coroutines.flow.Flow
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 11:10
   Project: EffectiveMobile Task
*/

interface CartUseCase {

    fun getCartList(): Flow<Result<CartData>>

    fun increment(increment: Pair<BasketItemData, Int>)

    fun decrement(decrement: Pair<BasketItemData, Int>)

    fun deleteItem(delete: Pair<BasketItemData, Int>)
}