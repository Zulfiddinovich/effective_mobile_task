package ru.effectivemobile.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import ru.effectivemobile.data.model.common.BasketItemData
import ru.effectivemobile.data.model.common.CartData
import ru.effectivemobile.domain.repository.MainRepository
import ru.effectivemobile.domain.usecase.CartUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class CartUseCaseImpl @Inject constructor(private val repository: MainRepository): CartUseCase {

    override fun getCartList() = flow<Result<CartData>> {
        repository.getCartList().collect {
            it.onSuccess {
                val response = it.toCartData()
                emit(Result.success(response))
            }
            it.onFailure {
                emit(Result.failure(it))
            }
        }
    }.flowOn(Dispatchers.IO)

    override fun increment(increment: Pair<BasketItemData, Int>) {
        repository.increment(Pair(increment.first.toBasketItemResponse(), increment.second))
    }

    override fun decrement(decrement: Pair<BasketItemData, Int>) {
        repository.decrement(Pair(decrement.first.toBasketItemResponse(), decrement.second))

    }

    override fun deleteItem(delete: Pair<BasketItemData, Int>) {
        repository.deleteItem(Pair(delete.first.toBasketItemResponse(), delete.second))
    }

}