package ru.effectivemobile.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import ru.effectivemobile.data.model.common.BestSellerData
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.data.model.common.SalesData
import ru.effectivemobile.domain.repository.MainRepository
import ru.effectivemobile.domain.usecase.MainUseCase
import javax.inject.Inject

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/

class MainUseCaseImpl @Inject constructor(private val repository: MainRepository): MainUseCase {

    override fun getCategoryList(): Flow<List<CategoryParent>> = repository.getCategoryList()

    override fun getHotSalesList() = flow<Result<List<SalesData>>>{
        repository.getHotSalesList().collect {
            it.onSuccess {
                val response = it.map { it.toSalesData() }
                emit(Result.success(response))
            }
            it.onFailure {
                emit(Result.failure(it))
            }
        }
    }.flowOn(Dispatchers.IO)

    override fun getBestSellerList() = flow<Result<List<BestSellerData>>>{
        repository.getBestSellerList().collect {
            it.onSuccess {
                val response = it.map { it.toBestSellerData() }
                emit(Result.success(response))
            }
            it.onFailure {
                emit(Result.failure(it))
            }
        }
    }.flowOn(Dispatchers.IO)


}