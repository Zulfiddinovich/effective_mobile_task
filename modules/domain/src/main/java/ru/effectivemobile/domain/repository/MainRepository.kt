package ru.effectivemobile.domain.repository

import kotlinx.coroutines.flow.Flow
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.data.model.response.*

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
interface MainRepository {

    fun getCategoryList(): Flow<List<CategoryParent>>

    fun getHotSalesList(): Flow<Result<List<SalesResponse>>>

    fun getBestSellerList(): Flow<Result<List<BestSellerResponse>>>

    fun getDetailedList(): Flow<Result<List<ProductDetailResponse>>>

    fun getCartList(): Flow<Result<CartResponse>>




    fun increment(increment: Pair<BasketItemResponse, Int>)

    fun decrement(decrement: Pair<BasketItemResponse, Int>)

    fun deleteItem(delete: Pair<BasketItemResponse, Int>)
}