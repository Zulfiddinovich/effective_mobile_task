package ru.effectivemobile.domain.usecase

import kotlinx.coroutines.flow.Flow
import ru.effectivemobile.data.model.common.BestSellerData
import ru.effectivemobile.data.model.common.CategoryParent
import ru.effectivemobile.data.model.common.SalesData

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 11:10
   Project: EffectiveMobile Task
*/
interface MainUseCase {

    fun getCategoryList(): Flow<List<CategoryParent>>

    fun getHotSalesList(): Flow<Result<List<SalesData>>>

    fun getBestSellerList(): Flow<Result<List<BestSellerData>>>


}