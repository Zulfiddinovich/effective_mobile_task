package ru.effectivemobile.task4.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.effectivemobile.task4.domain.repository.MainRepository
import ru.effectivemobile.task4.domain.repository.impl.MainRepositoryImpl
import javax.inject.Singleton

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 10:52
   Project: EffectiveMobile Task
*/
@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindMainRepository(impl: MainRepositoryImpl): MainRepository

}