package ru.effectivemobile.task4.domain.usecase

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow
import ru.effectivemobile.task4.data.model.common.BasketItemData
import ru.effectivemobile.task4.data.model.common.CartData
import ru.effectivemobile.task4.data.model.common.ProductData
import ru.effectivemobile.task4.data.model.response.ProductDetailResponse

/*
   Author: Zukhriddin Kamolov
   Created: 12.11.2022 at 11:10
   Project: EffectiveMobile Task
*/

interface DetailedProductUseCase {

    fun getDetailedList(): Flow<Result<List<ProductData>>>

}