package ru.effectivemobile.task4.presentation.ui.pages

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import ru.effectivemobile.task4.R
import ru.effectivemobile.task4.data.model.common.ProductData
import ru.effectivemobile.task4.databinding.FeaturesPageBinding

/*
   Author: Zukhriddin Kamolov
   Created: 06.11.2022 at 14:18
   Project: EffectiveMobile Task
*/
@AndroidEntryPoint
class FeaturesPage : Fragment(R.layout.features_page) {
    private val binding by viewBinding(FeaturesPageBinding::bind)
    private lateinit var productData: ProductData


    fun setProduct(data: ProductData){
        this.productData = data
//        this.productData.sd = data.sd
//        this.productData.images = data.images
//        this.productData.color = data.color
//        this.productData.ssd = data.ssd
//        this.productData.price = data.price
//        this.productData.rating = data.rating
//        this.productData.CPU = data.CPU
//        this.productData.isFavorites = data.isFavorites
//        this.productData.id = data.id
//        this.productData.camera = data.camera
//        this.productData.title = data.title
//        this.productData.capacity = data.capacity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.root.setOnClickListener {  }
    }
}